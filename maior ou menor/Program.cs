﻿using System;

namespace maior_ou_menor
{
    class Program
    {
        static void Main(string[] args)
        {

            maioroumenor maioroumenor = new maioroumenor();

            maioroumenor.Num1 = 1;
            maioroumenor.Num2 = 3;
            maioroumenor.Num3 = 3;

            // 321 - ok
            // 123 - ok
            // 231 - ok
            // 312 - ok
            // 213 - ok
            // 132 - ok

            int maior = maioroumenor.Maior();
            Console.WriteLine(maior);

            int menor = maioroumenor.Menor();
            Console.WriteLine(menor);

        }
    }
}